<?php
/**
 * Created by PhpStorm.
 * User: Zita
 * Date: 15.09.2015
 * Time: 12:41
 */
$config = require_once("config.php");


function curlExec($curlURL, array $curlPostFields, $curlError = false) {
    $ch = curl_init();
    curl_setopt_array($ch, [
        CURLOPT_URL => $curlURL,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_POSTFIELDS => $curlPostFields
    ]);
    if (!$curlError) {
        return curl_exec($ch);
    } else {
        return curl_error($ch);
    }

}
/*
function createPost ($postTitle, $postSummary, $postBody) {
        $id = sha1(time() . mt_rand(0, 10000));
        $arr = [
            "id" => $id,
            "title" => $postTitle,
            "summary" => $postSummary,
            "data" => date("d.m.Y H:i:s")
        ];
        file_put_contents(FILEJSON, json_encode($arr) . "\n" . file_get_contents(FILEJSON));
        $arr["body"] = $postBody;
        file_put_contents("db/" . $id . ".json", json_encode($arr));
}

*/