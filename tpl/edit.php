<?php require_once("header.php") ?>
        <div class="post">
            <form method="post">
                <div class="form-group">
                    <label>Post title</label>
                    <input type="text" class="form-control" name="title" value="<?php echo $post['title'] ?>"/>
                </div>
                <div class="form-group">
                    <label>Post summary</label>
                    <textarea class="form-control" name="summary"><?php echo $post['summary'] ?></textarea>
                </div>
                <div class="form-group">
                    <label>Post body</label>
                    <textarea rows="10" class="form-control" name="body" ><?php echo $post['body'] ?></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary form-control" value="Edit" />
                </div>
            </form>
            <hr />
        </div>
    </div>
<?php require_once("footer.php") ?>