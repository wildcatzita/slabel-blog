<ul class="nav nav-pills nav-stacked">
    <li <?= mb_substr($_SERVER["REQUEST_URI"], mb_strrpos($_SERVER["REQUEST_URI"],"/") + 1) === "index.php" || mb_substr($_SERVER["REQUEST_URI"], mb_strrpos($_SERVER["REQUEST_URI"],"/") + 1) === "" || mb_strrpos($_SERVER["REQUEST_URI"],'?page=') ? ' class="active"' : '' ?>>
        <a href="index.php">Main</a>
    </li>
    <?php if(!isset($_SESSION["auth"])) { ?>
    <li <?= mb_substr($_SERVER["REQUEST_URI"], mb_strrpos($_SERVER["REQUEST_URI"],"/") + 1) === "login.php" ? ' class="active"' : '' ?>>
        <a href="login.php">Login</a>
    </li>
    <?php } else  { ?>
    <li <?= mb_substr($_SERVER["REQUEST_URI"], mb_strrpos($_SERVER["REQUEST_URI"],"/") + 1) === "create.php" ? ' class="active"' : '' ?>>
        <a href="create.php">Create Post</a>
    </li>
        <?php if (isset($_SESSION["token"])) { ?>
    <li <?= mb_substr($_SERVER["REQUEST_URI"], mb_strrpos($_SERVER["REQUEST_URI"],"/") + 1) === "import_wall_vk.php" ? ' class="active"' : '' ?>>
        <a href="import_wall_vk.php">Import wall from VK</a>
    </li>
    <?php } ?>
        <?php if (isset($_SESSION["auth"])) {?>
    <li <?= mb_substr($_SERVER["REQUEST_URI"], mb_strrpos($_SERVER["REQUEST_URI"],"/") + 1) === "logout.php" ? ' class="active"' : '' ?>>
        <a href="logout.php">Logout</a>
    </li>
    <?php } }?>
</ul>