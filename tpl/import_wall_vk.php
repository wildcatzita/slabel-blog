<?php header('Content-Type: text/html; charset=utf-8');?>
<?php require_once("header.php") ?>
        <div class="post">
            <form method="post">
                <?php $i = 0;
                    foreach($wallPosts['response'] as $wallPost)  {
                        if (mb_strlen($wallPost['text']) != 0) {?>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="<?php echo $i?>" name="checked[]">
                                    <?php echo $wallPost['text'] ?>
                                    <input type="hidden" value="<?php echo $wallPost['text']?>" name="text[]">
                                    <input type="hidden" value="<?php echo $wallPost['id']?>" name="id[]">
                                </label>
                                <hr />
                            </div>
                    <?php $i++;
                        } else {
                            $i++;
                        }}?>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary form-control" value="Import" />
                </div>
            </form>
        </div>
    </div>
<?php require_once("footer.php") ?>