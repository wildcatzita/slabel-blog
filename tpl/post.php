<?php require_once("header.php") ?>
            <div class="post">
                <h2 class="post-title"><?php echo $post['title'] ?></h2>
                <blockquote class="blockquote-reverse">
                    <h3 class="post-subtitle">
                        <?php echo $post['body'] ?>
                    </h3>
                </blockquote>
                <p class="post-meta"><span class="glyphicon glyphicon-time"></span><?php echo $post['data'] ?>
                    <?php if (isset($_SESSION['auth'])) { ?>
                        <a href="<?php echo "edit.php?id=" . $_GET['id'] ?>" class="btn btn-success btn-sm pull-right" >Edit post</a>
                        <a href="<?php echo "delete.php?id=" . $_GET['id'] ?>" class="btn btn-danger btn-sm pull-right" style="margin-right: 15px">Delete post</a>
                    <?php }?>
                </p>
                <hr />
            </div>
        </div>
<?php require_once("footer.php") ?>