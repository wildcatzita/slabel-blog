<?php require_once("header.php") ?>
<?php foreach ($arrPosts as $item) {?>
    <div class="post">
        <h2 class="post-title"><?php echo $item['title'] ?></h2>

        <h3 class="post-subtitle"><?php echo $item['summary'] ?>
        </h3>

        <p class="post-meta"><span class="glyphicon glyphicon-time"></span><?php echo " " . $item['data'] ?>
            <a href="<?php echo "post.php?id=" . $item['id'] ?>" class="btn btn-primary btn-sm pull-right">Read More</a>
        </p>
        <hr/>
    </div>
<?php } ?>
<table class="table">
    <tbody>
    <tr>
        <td style="border: 0">
            <form method="post">
                <select name="prune_days">
                    <option value="all">За: все время</option>
                    <option value="week">За: 7 дней</option>
                    <option value="month">За: 30 дней</option>
                    <option value="year">За: 365 дней</option>
                </select>
                <input type="submit" value="ok">
            </form>
        </td>
        <td class="pull-right" style="border: 0; margin-top: -15px">
            <ul class="pagination pull-right" boundary-links="true">
                <?php if ($page > 1) {
                    $prev = $page - 1;
                    echo "<li><a href='?page=1&prune_days=$pruneDays' class='ng-binding'>First</a></li>";
                    echo "<li><a href='?page=$prev&prune_days=$pruneDays' class='ng-binding'>Previous</a></li>";
                } else {
                    if ($pages !== 0) {
                        echo "<li class='ng-scope disabled'><a href='?page=' class='ng-binding'>First</a></li>";
                        echo "<li class='ng-scope disabled'><a href='?page=' class='ng-binding'>Previous</a></li>";
                    }
                }
                for ($i = $page - 3; $i < $page + 3; $i++) {
                    if ($i > 0 && $i <= $pages) {
                        if ($i == $page) {
                            echo "<li class='active'><a href='?page=$i&prune_days=$pruneDays' class='ng-binding'>" . $i . "</a></li>";
                        } else {
                            echo "<li><a href='?page=$i&prune_days=$pruneDays' class='ng-binding'>" . $i . "</a></li>";
                        }
                    }
                }
                if ($page < $pages) {
                    $next = $page + 1;
                    echo "<li><a href='?page=$next&prune_days=$pruneDays'>Next</a></li>";
                    echo "<li><a href='?page=$pages&prune_days=$pruneDays'>Last</a></li>";
                } else {
                    if ($pages !== 0) {
                        echo "<li class='ng-scope disabled'><a href='?page=$pages&prune_days=$pruneDays'>Next</a></li>";
                        echo "<li class='ng-scope disabled'><a href='?page=$pages&prune_days=$pruneDays'>Last</a></li>";
                    }
                }
                ?>
            </ul>
        </td>
    </tr>
    </tbody>
</table>
</div>
<?php require_once("footer.php") ?>
