<?php

$config = require_once("config.php");
$client_id = $config['vk']['client_id'];
$client_secret = $config['vk']['client_secret'];
$redirect_uri = $config['vk']['redirect_uri'];
$pageTitle = "Login Page";
$blogTitle = $config['blogTitle'] . " | " . $pageTitle;

if(isset($_SESSION["auth"]))
    header("Location: index.php");

if(isset($_POST["username"]) && isset($_POST["password"])) {
    if($config["username"] === $_POST["username"] && $config["password"] === sha1($_POST["password"])) {
        $_SESSION["auth"] = true;
        header("Location: index.php");
    } else {
        $error = true;
    }
}

$url = 'https://oauth.vk.com/authorize';
$params = [
    'client_id'     => $client_id,
    'redirect_uri'  => $redirect_uri,
    'display' => 'popup',
    'scope' => 'email,offline,wall,friends',
    'response_type' => 'code'
    ];

$link = $url . '?' . urldecode(http_build_query($params));

require_once("tpl/login.php");
