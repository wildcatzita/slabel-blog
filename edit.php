<?php
$config = require_once("config.php");
$pageTitle = "Edit Post";
$blogTitle = $config['blogTitle'] . " | " . $pageTitle;
$fileJSON = $config['filejson'];
$id = $_GET['id'];
$postFile = "db/" . $id . ".json";
require_once('FileDB.php');

if (file_exists($postFile) && file_exists($fileJSON)) {
    $post = json_decode(file_get_contents($postFile), true);
    require_once("tpl/edit.php");
    if (isset($_POST["summary"]) && isset($_POST["title"]) && isset($_POST["body"])) {
        $f = new FileDB($fileJSON, 'r+');
        $f->updatePost($id, $_POST["title"], $_POST["summary"], $_POST["body"]);
        header("Location: post.php?id={$id}");
    }
} else {
    header("Location: index.php");
}