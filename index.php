<?php

$config = require_once("config.php");
$pageSize = $config["pageSize"];
$page = $config['page'];
$pages = $config['pages'];
$fileJSON = $config['filejson'];
$pageTitle = "Main";
$blogTitle = $config['blogTitle'] . " | " . $pageTitle;
$pruneDays = 0;
require_once("func.php");
require_once("FileDB.php");
$f = new FileDB($fileJSON, "r", $pageSize);
//echo $f;
if (file_exists($fileJSON)) {
    if (isset($_POST['prune_days'])) {
        $pruneDays = $f->$_POST['prune_days'];
        $page = 1;
        $size = $f->postsCount();
        $pages = ceil($size / $pageSize);
        $arrPosts = $f->getPosts($page);
        //var_dump($pruneDays, $size, $pages, count($arrPosts));
        //var_dump("isset(POST[prune_days])");
        unset($_GET['prune_days']);
        Header("Location: index.php?page=1&prune_days=$pruneDays");
    }
    if (isset($_GET['page']) && is_numeric($_GET['page']) && $_GET['page'] > 0) {
        $page = $_GET['page'];
        if (isset($_GET['prune_days'])) {
            $pruneDays = $_GET['prune_days'];
            //var_dump("pruneDays");
        }
        $size = $f->postsCount();
        $pages = ceil($size / $pageSize);
        $arrPosts = $f->getPosts($page, $pruneDays);
    }
    //$arrPosts = $f->getPosts($page, $pruneDays);
    if (!isset($_POST['prune_days']) && !isset($_GET['prune_days'])) {
        //var_dump("!isset(POST[prune_days])");
        $size = $f->postsCount();
        $pages = ceil($size / $pageSize);
        $arrPosts = $f->getPosts($page);
    }
}
include_once("tpl/index.php");


