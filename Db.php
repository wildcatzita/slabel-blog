<?php

abstract class Db {
    abstract function createPost($title, $summary, $body);
    abstract function updatePost($postId, $title, $summary, $body);
    abstract function getPosts($page, $prune_days);
    abstract function postsCount();
    abstract function deletePost($postId);
}