<?php
$config = require_once("config.php");
$client_id = $config['vk']['client_id'];
$client_secret = $config['vk']['client_secret'];
$redirect_uri = $config['vk']['redirect_uri'];

require_once("func.php");

if (isset($_GET['code'])) {
    $_SESSION['codevk'] = $_GET['code'];
    $arrPostFields = [
        "client_id" => $client_id,
        "client_secret" => $client_secret,
        "code" => $_GET['code'],
        "redirect_uri" => $redirect_uri ];
    $token = curlExec('https://oauth.vk.com/access_token', $arrPostFields);
    $error = curlExec('https://oauth.vk.com/access_token', $arrPostFields, true);
    if ($token) {
        if (is_string($token)) {
            $token = json_decode($token, true);
            $_SESSION['auth'] = true;
            $_SESSION['token'] = $token['access_token'];
            $_SESSION['user_id'] = $token['user_id'];
            header("Location: index.php");
        }
    }
}
