<?php

require_once('Db.php');


class FileDB extends Db{
    private $arr = [];
    private $id;
    private $pruneDays;
    private $pageSize;
    private $filePath;
    private $fileLink = false;
    const FORMAT = "d.m.Y H:i:s";

    public function __construct($filePath, $mode = 'r+', $pageSize = 1) {
        //var_dump("New obj");
        $this->pageSize = $pageSize;
        $this->filePath = $filePath;
        if (isset($_GET['prune_days'])) {
            //var_dump('construct()', $_GET['prune_days']);
            $this->pruneDays = $_GET['prune_days'];
        }
        if(file_exists($this->filePath)) {
            $this->fileLink = fopen($this->filePath, $mode);
        } else {
            die('File not exists');
        }
    }

    public function createPost($postTitle, $postSummary, $postBody) {
        $this->id = sha1(time() . mt_rand(0, 10000));
        $this->arr = [
            "id" => $this->id,
            "title" => $postTitle,
            "summary" => $postSummary,
            "data" => date(self::FORMAT)
        ];
        file_put_contents($this->filePath, json_encode($this->arr) . "\n" . file_get_contents($this->filePath));
        $this->arr["body"] = $postBody;
        file_put_contents("db/" . $this->id . ".json", json_encode($this->arr));
    }

    public function updatePost($postID, $postTitle, $postSummary, $postBody) {
        $postFile = "db/" . $postID . ".json";
        $tmpFile = tempnam("db", 'db_');
        $tF = fopen($tmpFile, "w");
        while (!feof($this->fileLink)) {
            $str = fgets($this->fileLink);
            if (strpos($str, $postID)) {
                $line = json_decode($str, true);
                if ($line['id'] == $postID) {
                    $this->arr = [
                        "id" => $postID,
                        "title" => $postTitle,
                        "summary" => $postSummary,
                        "data" => date(self::FORMAT)
                    ];
                    $str = json_encode($this->arr) . "\n";
                }
            }
            fwrite($tF, $str);
        }
        fclose($tF);
        fclose($this->fileLink);
        rename($tmpFile, $this->filePath);
        $flag = unlink($postFile);
        if ($flag) {
            $this->arr["body"] = $postBody;
            file_put_contents("db/" . $postID . ".json", json_encode($this->arr));
        }
    }

    public function deletePost($postId) {
        $tmpFile = tempnam("db", 'db_');
        $tF = fopen($tmpFile, "w");
        $postFile = "db/" . $postId . ".json";
        if(file_exists($postFile)) {
            while (!feof($this->fileLink)) {
                $str = fgets($this->fileLink);
                if (strpos($str, $postId)) {
                    $line = json_decode($str, true);
                    if ($line['id'] == $postId) {
                        continue;
                    }
                }
                fwrite($tF, $str);
            }
            fclose($tF);
            fclose($this->fileLink);
            unlink($postFile);
            rename($tmpFile, $this->filePath);
            chmod($this->filePath, 0777);
        }
    }

    public function getPosts($page, $prune_days = 0) {
        $this->arr = [];
        $posts = file($this->filePath);
        //var_dump($prune_days);
        $now = new DateTime();
        //echo intval($dt->diff($now)->format("%d"));
        $offset = ($page - 1) * $this->pageSize;
        $end = $offset + $this->pageSize;
        for ($i = 0; $i < $offset; $i++) {
            fgets($this->fileLink);
        }
        for ($i = $offset; ($i < $end && !@feof($this->fileLink)); $i++) {
            $tmp = json_decode(fgets($this->fileLink), true);
            $dt = new DateTime($tmp['data']);
            $diffPruneDays = intval($dt->diff($now)->format("%a"));
            if (!is_null($tmp) && $prune_days == 0) {
                $this->arr[] = $tmp;
            } else if (!is_null($tmp) && $diffPruneDays <= $prune_days) {
                $this->arr[] = $tmp;
            }
        }
        return $this->arr;
    }

    /*
    public function getPosts($page) {
        $tmp = [];
        $posts = file($this->filePath);
        //var_dump(count($this->arr));
        $offset = ($page - 1) * $this->pageSize;
        $end = $offset + $this->pageSize;
        var_dump($offset, $end);
        for ($i = $offset; $i < $end; $i++) {
            $tmp[] = $this->arr[$i];
        }
        return $tmp;
    }
    */
    public function __get($param) {
        //var_dump($param);
        switch($param) {
            case 'week' : $this->pruneDays = 7; break;
            case 'month' : $this->pruneDays = 30; break;
            case 'year' : $this->pruneDays = 365; break;
            default: $this->pruneDays = 0; break;
        }
        return $this->pruneDays;
    }

    public function postsCount() {
        $posts = file($this->filePath);
        $now = new DateTime();
        $this->arr = [];
        //var_dump($this->pruneDays);
        if ($this->pruneDays == 0) {
            foreach($posts as $item) {
                $this->arr[] = json_decode($item, true);
            }
            return count($this->arr);
        }
        foreach($posts as $item) {
            $item = json_decode($item, true);
            $dt = new DateTime($item['data']);
            $diffPruneDays = intval($dt->diff($now)->format("%a"));
            if ($diffPruneDays <= $this->pruneDays) {
                //var_dump($diffPruneDays, $dt);
                $this->arr[] = $item;
            }
        }
        return count($this->arr);
    }

    public function __toString() {
        return 'Posts count = ' . $this->postsCount();
    }
}
