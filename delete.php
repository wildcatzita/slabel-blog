<?php
$config = require_once("config.php");
$fileJSON = $config['filejson'];
require_once('FileDB.php');

if (isset($_GET['id']) && file_exists($fileJSON)) {
    $f = new FileDB($fileJSON, 'r+');
    $f->deletePost($_GET['id']);
}
header("Location: index.php");