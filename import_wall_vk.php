<?php
/**
 * Created by PhpStorm.
 * User: Zita
 * Date: 12.09.2015
 * Time: 11:37
 */
$config = require_once("config.php");
$pageTitle = "Import posts from VK wall";
$blogTitle = $config['blogTitle'] . " | " . $pageTitle;
$fileJSON = $config['filejson'];

require_once("func.php");
require_once("FileDB.php");

if (isset($_SESSION['token']) && isset($_SESSION['auth']) && $_SESSION['user_id']) {
    $arrPostFields = [
            "owner_id" => $_SESSION['user_id'],
            "access_token" => $_SESSION['token'],
            "count" => 10,
            "filter" => "all"
        ];
    $wallPosts = curlExec('https://api.vk.com/method/wall.get', $arrPostFields);
    $wallPosts = json_decode($wallPosts, true);
} else {
    if (!headers_sent()) {
        header('Location: index.php');
        exit;
    }
}

if (isset($_POST['id']) && isset($_POST['text']) && isset($_POST['checked'])) {
    $i = 0;
    $f = new FileDB($fileJSON);
    foreach ($wallPosts['response'] as $wallPost) {
        for ($j = 0; $j < count($_POST['checked']); $j++) {
            if ($i == $_POST['checked'][$j]) {
                $f->createPost($wallPost['id'], mb_substr($wallPost['text'], 0, 200), $wallPost['text']);
            }
        }
        $i++;
    }
    if (!headers_sent()) {
        header('Location: index.php');
        exit;
    }
}
include_once("tpl/import_wall_vk.php");