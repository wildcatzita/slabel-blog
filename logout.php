<?php
$config = require_once("config.php");
if (isset($_SESSION['auth'])) {
    unset($_SESSION['auth']);
    if (isset($_SESSION['token'])) {
        unset($_SESSION['token'], $_SESSION['user_id'], $_SESSION['codevk'], $_POST['text'], $_POST['checked'], $_POST['id']);
    }
    header("Location: index.php");
}
