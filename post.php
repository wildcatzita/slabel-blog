<?php
$config = require_once("config.php");
$pageTitle = "Post";
$blogTitle = $config['blogTitle'] . " | " . $pageTitle;
$filename = "db/" . $_GET["id"] . ".json";
if (file_exists($filename)) {
    $post = json_decode(file_get_contents($filename), true);
    require_once("tpl/post.php");
} else {
    header("Location: index.php");
}




